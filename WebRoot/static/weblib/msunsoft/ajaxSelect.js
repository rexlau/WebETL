﻿

var ZY$$$COUNTFORALL = 1;

(function (window) {
    var zlist = [];
    var RoundNumericType = {
        Ceil: 1, //向上取整
        Floor: 2, //向下取证
        Round: 3, //四舍五入
        None: 4, //不取整
        Fifty: 5//舍入到最近的5角(7.5毛以下到5毛)
    };
    var RoundType = RoundNumericType.Round;
    var RoundLen = 1;
    var RoundParamNumeric = Math.pow(10, RoundLen);
    var zQuery = {
        SetRoundParam: function (roundType, RoundParam) {
            /// <summary>设置四舍五入计算的参数</summary>
            /// <param name="RoundType" type="numeric">四舍五入的类型 参见RoundNumericType</param>
            /// <param name="RoundParam" type="numeric">舍入到的小数位数</param>
            RoundLen = RoundParam;
            RoundType = roundType;
            RoundParamNumeric = Math.pow(10, RoundLen);

        }, //SetRoundParam
        CountNumericForRound: function (numeric) {
            /// <summary>计算四舍五入</summary>
            var numora = numeric;
            if (parseInt(numeric) == numeric) {
                return numeric;
            }
            numeric = numeric * RoundParamNumeric;
            numeric = zy$.GetFixNum(numeric);
            var result = 0;
            var rtp = RoundType;
            if (numora < 0) {

                if (rtp == RoundNumericType.Ceil) {
                    rtp = RoundNumericType.Floor;
                }
                else {
                    if (rtp == RoundNumericType.Floor) {
                        rtp = RoundNumericType.Ceil;
                    }
                }

            }
            switch (rtp) {
                case RoundNumericType.Ceil:
                    result = Math.ceil(numeric) / RoundParamNumeric;
                    break;

                case RoundNumericType.Floor:
                    result = Math.floor(numeric) / RoundParamNumeric;
                    break;

                case RoundNumericType.Round:
                    if (numeric < 0) {
                        numeric = 0 - numeric;
                        result = Math.round(numeric) / RoundParamNumeric;
                        numeric = 0 - numeric;
                        result = 0 - result;
                    } else {
                        result = Math.round(numeric) / RoundParamNumeric;
                    }


                    break;

                case RoundNumericType.None:
                    result = numeric = numora;
                    break;
                case RoundNumericType.Fifty:
                    var divv = numora - parseInt(numora);
                    if (divv > 0.75) {
                        return parseInt(numora) + 1;
                    }
                    if (divv == 0) {
                        return numora;
                    }
                    if (divv <= 0.75) {
                        return parseInt(numora) + 0.5;
                    }
                    break;
            }
            return result;
        }, //CountNumericForRound
        SetCookie: function (name, value, expires) {
            /// <summary>添加一个cookies</summary>
            /// <param name="name" type="string">名称</param>
            /// <param name="value" type="string">值</param>
            /// <param name="expires" type="numeric">过期日期 天</param>
            /// <returns type="void" />
            var expDays = expires * 24 * 60 * 60 * 1000;
            var expDate = new Date();
            expDate.setTime(expDate.getTime() + expDays);
            var expString = ((expires == null) ? "" : (";expires=" + expDate.toGMTString()));
            document.cookie = name + "=" + escape(value) + expString;
        }, //SetCookie
        GetCookie: function (name) {
            /// <summary>获取cookies </summary>
            /// <param name="name" type="string">名称 </param>
            /// <returns type="string" />
            var result = null;
            var myCookie = document.cookie + ";";
            var searchName = name + "=";
            var startOfCookie = myCookie.indexOf(searchName);
            var endOfCookie;
            if (startOfCookie != -1) {
                startOfCookie += searchName.length;
                endOfCookie = myCookie.indexOf(";", startOfCookie);
                result = unescape(myCookie.substring(startOfCookie, endOfCookie));
            }
            return result;

        }, //GetCookie
        ClearCookie: function (name) {
            /// <summary>清除cookie</summary>
            /// <param name="name" type="string">名称</param>
            /// <returns type="void" />

            var ThreeDays = 3 * 24 * 60 * 60 * 1000;
            var expDate = new Date();
            expDate.setTime(expDate.getTime() - ThreeDays);
            document.cookie = name + "=;expires=" + expDate.toGMTString();
        }, //ClearCookie
        CheckTimeStr: function (timestr) {
            /// <summary>检测 yyyy-HH-dd HH:mm:ss 时间格式的准确性</summary>
            /// <param name="timestr" type="string"> yyyy-HH-dd HH:mm:ss 时间格式的字符串</param>
            /// <returns type="boolean" />

            var regstr = /^([1-9]\d{3})-(1[0-2]|0[1-9])-([0-2]\d|3[0-1]) ([0-1][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/;
            var reg = new RegExp(regstr);
            if (!reg.test(timestr)) {
                return false;
            }
            //str = timestr;
            //判断日期是否正确
            //year
            var y = parseInt(parseFloat(timestr.substring(0, 4)));
            var m = parseInt(parseFloat(timestr.substring(5, 7)));
            var d = parseInt(parseFloat(timestr.substring(8, 10)));
            var isLeap = false;
            if (((y % 4 == 0) && (y % 100 != 0)) || (y % 400 == 0)) {
                isLeap = true;
            }
            if (m == 4 || m == 6 || m == 9 || m == 11) {
                if (d > 30) {
                    return false;
                }
            }
            if (m == 2) {
                if (isLeap) {
                    if (d > 29) {
                        return false;
                    }
                }
                else {
                    if (d > 28) {
                        return false;
                    }
                }
            }
            return true;
        }, //CheckTimeStr
        DateParse: function (datetimestr) {
            /// <summary>通过字符串获取时间</summary>
            /// <param name="name" type="string">时间字符串</param>
            /// <returns type="string" />
            datetimestr = datetimestr.replace(/-/g, "/");
            var date = new Date(datetimestr);
            //date = new Date( Date.parse(datetimestr));
            //date.setTime(Date.parse(datetimestr));
            return date;
        }, //DateParse
        CreateSelectAjax: function (textid, ajaxPage, param, doSelectFun, limitLen, countShowEveryTime, isNotOrderBuInput) {
            /// <summary>
            /// ajax筛选客户端函数,基于JQuery框架 和 ZQuery 框架
            /// 要求回调形势： <table><tr name="内容的id">表头行 行1：标题行 其中 index： 0 : 拼音简码 1： 名称等等。。。可以指定</tr>...对应的内容，要求是按照拼音编码从小到大排序的</table>
            /// 其中第一行的表头的每个单元格都有样式className eg： 偶 13列： ajaxSelectTd0 。。。。。 ajaxSelectTd12 可以用 指定div样式的 下层样式来设置
            /// </summary>
            /// <param name="textid" type="string">绑定的文本框id</param>
            /// <param name="ajaxPage" type="string">回调页面的地址</param>
            /// <param name="param" type="string">回调页面使用的参数</param>
            /// <param name="doSelectFun" type="function">
            /// 确认选择内容时执行的函数
            /// 函数的参数：eg: {id:'2321', content:['nha','你好啊'......] } 注： content的内容是表格选中的单元格的各个列的innerText 
            /// </param>
            /// <param name="limitLen" type="numeric">开始回传的最小字符串长度可以为空默认是1</param>
            /// <param name="countShowEveryTime" type="boolean">是否在每次显示时重新计算位置</param>
            /// <param name="isNotOrderBuInput" type="boolean">如果没有使用inputcode排序则职位true</param>
            /// <returns type="ZyAjaxContentSelect" />
            return new ZyAjaxContentSelect(textid, ajaxPage, param, doSelectFun, limitLen, countShowEveryTime, isNotOrderBuInput);
        }, //CreateSelectAjax
        CreateScriptSrc: function (url) {
            /// <summary>添加js引用，防止缓存出现</summary>
            /// <param name="url" type="string">js文件地址</param>
            /// <returns type="void" />
            document.writeln(" <script type=\"text/javascript\" language=\"javascript\" src=" + url + "?" + (new Date()).getTime() + "></script>");
        }, //CreateScriptSrc
        CreateZYCombox: function (selectid, page, param, className, paramArr, json, endDoFun) {
            /// <summary>众阳select控件</summary>
            /// <param name="selectid" type="string">要扩充的select控件的id </param>
            /// <param name="page" type="string">要获取数据的pageurl</param>
            /// <param name="param" type="string">要给后台页传递的参数</param>
            /// <param name="className" type="string">遮挡div的样式className</param>
            /// <param name="paramArr" type="Array">要传递的第二个数组以键值对的形式来封装参数</param>
            /// <param name="json" type="string">如果是使用指定的json形式初始化则将该项赋值为非空的对应的json字符串</param>
            /// <param name="endDoFun" type="function">初始化完毕后调用的函数</param>
            /// <returns type="void" />
            return new zySelectCombox(selectid, page, param, className, paramArr, json, endDoFun);
        }, //CreateZYCombox
        CreateZYGrid: function (table, divContainer, divMain, isFix, isChangeColWidth) {
            /// <summary>众阳表格转化工具需要JQuery的支持</summary>
            /// <param name="table" type="DOMObject">
            ///  要使用该工具转化的表格,如果是null则需要自己调用相应的API来创建一个表格
            ///  表格的要求 如果没有thead 则吗默认取第一行为表头,转换i后只保留表格中个元素的Id和Name属性
            ///  如果表头有两行或者以上，则不允许排序，自定义列宽
            /// </param>
            /// <param name="divContainer" type="DOMObject">存放该表格的外层的DIV对象，要求该对象内部只有一个表格对象</param>
            /// <param name="divMain" type="Dom">在表格外面的div 如果该值不为空，说明table外面有两侧 div 最外层是divContainer 中间是divMain</param>
            /// <param name="isFix" type="boolean">是否启用表头固定</param>
            /// <param name="isChangeColWidth" type="boolean">是否启用拖动列宽（影像效率）</param>

            return new ZYTableGrid(table, divContainer, divMain, isFix, isChangeColWidth)

        }, //endCreateZYGridk
        CancerBubble: function (e) {
            /// <summary>阻止事件冒泡</summary>
            /// <param name="e" type="Event">非Ie浏览器需要</param>
            if (e && e.stopPropagation) {
                e.stopPropagation();
            }
            else {
                window.event.cancelBubble = true;
            }
        }, //CancerBubble
        CheckIsInt: function (num) {
            /// <summary>严格检查一个对象是否是整数</summary>
            /// <param name="num" type="var">要检测的string或者 numeric</param>
            /// <returns type="boolean" />
            if (isNaN(num)) {
                return false;
            }
            if (parseFloat(num) + "" != num + "") {
                return false;
            }
            return (parseFloat(num) == parseInt(parseFloat(num)));
        }, //CheckIsInt
        CheckIsNumeric: function (num) {
            /// <summary>检查一个对象是否是数值</summary>
            /// <param name="name" type="var">检查一个对象是否是数值</param>
            /// <returns type="boolean" />
            if (isNaN(num)) {
                return false;
            }
            return true;
        }, //CheckIsNumeric
        GetFixNum: function (number) {
            /// <summary>获取准确数值消除浮点误差</summary>
            /// <param name="number" type="numeric">要消除的数</param>
            /// <returns type="numeric" />
            //            number += 0.000000001;
            //            if (number == parseInt(number)) {
            //                return number;
            //            }
            //            number = number * 1000000;
            //            number = Math.round(number);
            //            number = number / 1000000;
            //            var fixNum = number + "";
            //            if (fixNum.indexOf(".")==-1) {
            //                return number;
            //            }
            //            var pointRightLen = fixNum.length - fixNum.indexOf(".") - 1;
            //            if(pointRightLen>4) {
            //                fixNum = number.toFixed(4);
            //                number = parseFloat(fixNum);
            //            }
            var fixNum = number.toFixed(4);
            number = parseFloat(fixNum);
            return number;
        }, //GetFixNum
        CloseWindow: function () {
            window.opener = window.self;
            window.open('', '_self');
            window.close();
        }, //CloseWindow
        GetUrlPara: function () {
            /// <summary>获取打开新页面时候必须有的参数一般加在？后面，第一个字符不带&</summary>
            return "userSysId=" + ZYCOMMVALUE_USERSYSID + "&tsCheck=" + GetCurentTs(ZYCOMMVALUE_USERSYSID);
        }, //end GetUrlPara
        GetUrlParamStr: function (json) {
            /// <summary>获取打开新页面需要传递参数时封装成的参数字符串，reDirectSys页面和Default页面接受</summary>
            /// <param name="json" type="Json">参数组成的数据结构：{id:'123',name:'acsdf'}</param>
            var arr = [];
            var index = 0;
            for (var item in json) {
                if (index != 0) {
                    arr.push("$");
                }
                arr.push(item);
                arr.push("|");
                arr.push(json[item]);
                index++;
            }
            return arr.join("");
        }, //end GetUrlParamStr
        GetUrlToPage: function (systemId, pageName, paramJson) {
            /// <summary>获取跳转到某个其他系统页面的url</summary>
            /// <param name="systemId" type="int">目标系统编号</param>
            /// <param name="pageName" type="string">页面名称</param>
            /// <param name="paramJson" type="json">参数组成的数据结构：{id:'123',name:'acsdf'}</param>

            var url = "";
            url = "/WebComm/reDirectSys.aspx?" + zQuery.GetUrlPara();
            try {
                if (ZYCURWORKSTATIONID123) {
                    url += "&workstationId=" + ZYCURWORKSTATIONID123;
                }
            }
            catch (e) {
            }
            url += "&systemId=" + systemId + "&isFull=1&page=" + pageName + "&param=" + zQuery.GetUrlParamStr(paramJson);
            return url;
        }, //end GetUrlToPage,
        AddWdataPickToElement: function (Ele, DateFormat, TimeDiv) {
            /// <summary>像某个空间添加日期空间必须引用jquery，my97日期控件</summary>
            /// <param name="Ele" type="String">要触发显示控件的元素或者其ID</param>
            /// <param name="DateFormat" type="String">日期格式</param>
            /// <param name="TimeDiv" type="int">默认时间与当前时间的差（向前）(秒) 获取时间字符串</param>
            if (typeof (Ele) == "string") {
                Ele = document.getElementById(Ele);
            }
            var id = Ele;
            if (Ele.id) {
                id = Ele.id;
            }
            $(Ele).focus(function () {

                WdatePicker({
                    el: id,
                    isShowClear: false,
                    dateFmt: DateFormat,
                    errDealMode: 1
                });
                $dp.hide();
            });
            var img = $('<img src="/WebComm/CommScripts/My97DatePicker/skin/datePicker.gif" width="16" height="22" align="absmiddle" alt="显示日历" title="显示日历" />');
            if ($(Ele).next().attr("src") != undefined && $(Ele).next().attr("src") != "" && $(Ele).next().attr("src").indexOf("/WebComm/CommScripts/My97DatePicker/skin/datePicker.gif") >= 0) {
                img = $(Ele).next();
                img.unbind("click");

            } else {
                $(Ele).after(img);
            }

            $(Ele).after(img);
            if (typeof (TimeDiv) == "string") {
                Ele.value = zy$.DateParse(TimeDiv).format(DateFormat);
            }
            else {
                Ele.value = (new Date((new Date()) - 1000 * TimeDiv)).format(DateFormat);
            }

            img.click(function (e) {
                // var ev = e || window.event;
                WdatePicker({
                    el: Ele,
                    isShowClear: false,
                    dateFmt: DateFormat,
                    errDealMode: 2,
                    autoPickDate: true
                });
            });
        }, //end AddWdataPickToElement


        AddWdataPickToElementForInput: function (Ele, DateFormat, TimeDiv) {
            /// <summary>像某个空间添加日期空间必须引用jquery，my97日期控件</summary>
            /// <param name="Ele" type="String">要触发显示控件的元素或者其ID</param>
            /// <param name="DateFormat" type="String">日期格式</param>
            /// <param name="TimeDiv" type="int">默认时间与当前时间的差（向前）(秒) 获取时间字符串</param>
            if (typeof (Ele) == "string") {
                Ele = document.getElementById(Ele);
            }
            var id = Ele;
            if (Ele.id) {
                id = Ele.id;
            }
            $(Ele).click(function () {
                if (this.value != "" && this.value != null && this.value != undefined) {
                    $(this).attr("oldDate", this.value);
                    this.value = "";
                }
            });
            $(Ele).focus(function () {

                WdatePicker({
                    el: id,
                    isShowClear: false,
                    dateFmt: DateFormat,
                    errDealMode: 1,
                    position: { left: -1000, top: -500 }
                });
                $dp.hide();
            });
            $(Ele).blur(function () {
                if (this.value == "" || this.value == null || this.value == undefined) {
                    this.value = $(this).attr("oldDate");
                }
            });
            var img = $('<img src="/WebComm/CommScripts/My97DatePicker/skin/datePicker.gif" width="16" height="22" align="absmiddle" alt="显示日历" title="显示日历" />');
            if ($(Ele).next().attr("src") != undefined && $(Ele).next().attr("src") != "" && $(Ele).next().attr("src").index("/WebComm/CommScripts/My97DatePicker/skin/datePicker.gif") >= 0) {
                img = $(Ele).next();
                img.unbind("click");

            } else {
                $(Ele).after(img);
            }

            $(Ele).after(img);
            if (typeof (TimeDiv) == "string") {
                Ele.value = zy$.DateParse(TimeDiv).format(DateFormat);
            }
            else {
                Ele.value = (new Date((new Date()) - 1000 * TimeDiv)).format(DateFormat);
            }

            img.click(function (e) {
                // var ev = e || window.event;
                WdatePicker({
                    el: Ele,
                    isShowClear: false,
                    dateFmt: DateFormat,
                    errDealMode: 2,
                    autoPickDate: true
                });
            });
        }, //end AddWdataPickToElement


        GetAge: function (BirthDay, RefDay) {
            /// <summary>计算患者年龄</summary>
            /// <param name="BirthDay">出生日期</param>
            /// <param name="RefDay">基准日期</param>

            if (typeof BirthDay == 'string') {
                BirthDay = StringToDate(BirthDay);
            }

            if (typeof RefDay == 'string') {
                RefDay = StringToDate(RefDay);
            }

            var totalDays = BirthDay.DaysBetween('d', RefDay);

            var ageStr = "";
            var ageUnit = "";
            var ageVal = 0;

            if (totalDays / 365 > 1) {
                ageUnit = '岁';
                ageVal = totalDays / 365;
            }
            else if (totalDays / 30 > 1) {
                ageUnit = '月';
                ageVal = totalDays / 30;
            }
            else if (totalDays > 1) {
                ageUnit = '天';
                ageVal = totalDays;
            }
            else {
                var totalHours = BirthDay.DaysBetween('h', RefDay);
                if (totalHours > 1) {
                    ageUnit = '小时';
                    ageVal = totalHours;
                }
                else {
                    var totalMinutes = BirthDay.DaysBetween('n', RefDay);

                    ageUnit = '分钟';
                    ageVal = totalMinutes;
                }
            }

            ageStr = Math.round(ageVal) + ageUnit;
            return ageStr;
        }   // end GetAge
    }; //zQuery
    window.zy$ = zQuery;
})(window);






// #region ZyAjaxContentSelect
function ZyAjaxContentSelect(textid, ajaxPage, doSelectFun, limitLen, countShowEveryTime, isNotOrderBuInput) {
    /// <summary>
    /// ajax筛选客户端函数,基于JQuery框架 和 ZQuery 框架
    /// 要求回调形势： <table><tr name="内容的id">表头行 行1：标题行 其中 index： 0 : 拼音简码 1： 名称等等。。。可以指定</tr>...对应的内容，要求是按照拼音编码从小到大排序的</table>
    /// 其中第一行的表头的每个单元格都有样式className eg： 偶 13列： ajaxSelectTd0 。。。。。 ajaxSelectTd12 可以用 指定div样式的 下层样式来设置
    /// </summary>
    /// <param name="textid" type="string">绑定的文本框id</param>
    /// <param name="ajaxPage" type="string">回调页面的地址</param>
    /// <param name="doSelectFun" type="function">
    /// 确认选择内容时执行的函数
    /// 函数的参数：eg: {id:'2321', content:['nha','你好啊'......] } 注： content的内容是表格选中的单元格的各个列的innerText 
    /// </param>
    /// <param name="limitLen" type="numeric">开始回传的最小字符串长度可以为空默认是1</param>
    /// <param name="countShowEveryTime" type="boolean">是否在每次显示时重新计算位置</param>
    /// <param name="isNotOrderBuInput" type="boolean">如果没有使用inputcode排序则职位true</param>
    /// <returns type="string" />
    ZY$$$COUNTFORALL++;
    var othis = this; ////自身指代
    this.inputTextId = textid;
    this.pageurl = ajaxPage;
    this.isNotOrderBuInput = isNotOrderBuInput || false;
    this.limitLen = limitLen || 1;
    this.ajaxId = "ajaxselectzy" + ZY$$$COUNTFORALL + textid; // ajax前后台传递id
    ZY$$$COUNTFORALL++;
    this.doSelectFun = doSelectFun; // 选择内容时执行的函数
    this.inputDom = null; //输入框HTML的DOM对象
    this.divDom = null; //容器div的HTML 的 DOM 对象
    this.divDomIn = null; //存放展示内容的容器
    this.lastInputId = ""; //上次发送的检索字符串
    this.isOnSelectSelf = true; //是否开启自行检索
    this.isSelecting = false; //是否正在执行筛选
    this.Width = $("#" + this.inputTextId).width();
    this.Height = 220;
    this.containerClassName = "ajaxSelectClassZY";
    this.csEveryTime = false;
    this.hidiframeHTML = "";
    this.headtr = null; //首行
    this.Enabled = true; //启用标志
    if (countShowEveryTime) {
        this.csEveryTime = true;
    }


    /*类库属性*/
    //附加信息
    this.Addtion = "";
    //是否自动离开焦点隐藏
    this.AutoHiden = true;
    //是否检索为空时焦点隐藏
    this.IsEmptyHiden = false;
    //在回车时候必定执行的函数
    this.DoFunEnter = null;
    //在控件禁用的时候回车调用函数
    this.DoFunEnterWhenDisable = null;
    //是否正在显示
    this.IsShow = false;
    //设置提示文本框宽度，默认和输入文本框宽度相同
    this.setWidth = function (width) {
        othis.Width = width;
        $(othis.divDom).width(othis.Width);
        $(othis.divDomIn).width(othis.Width);
        //$(othis.divDom).children("iframe").width(othis.Width);
    };
    //设置提示框高度默认300
    this.setHeight = function (height) {
        othis.Height = height;
        $(othis.divDom).height(othis.Height);
        $(othis.divDomIn).height(othis.Height);
        // $(othis.divDom).children("iframe").height(othis.Height);
    };
    //设置从多少字节长度开始从客户端进行内容的筛选
    this.HowLenStrWhereSelectByClient = this.limitLen + 10;
    //设置容器div的className 可以根据它来灵活的设置显示的样式
    this.setContainerClassName = function (className) {
        $(othis.divDomIn).removeClass(othis.containerClassName);
        othis.containerClassName = className;
        $(othis.divDomIn).addClass(othis.containerClassName);
    };
    //设置选中表格行的样式
    this.selectTrClassName = "ajaxSelectClassTr";
    //表头样式
    this.tableHeadClass = "ajaxSelectHead";
    //设置显示的位置 格式{x:12,y:21}，null时按照当前input的位置的下面定位
    this.showPos = function (a) {
        var containerdiv = $(othis.divDom);
        containerdiv.css("left", a.x);
        containerdiv.css("top", a.y);
    };
    //拼音显示所在的单元格index
    this.InputCodeShowCol = 0;
    //当前正在选中的tr
    this.SelectTr = null;

    //初始化
    this.init = function () {

        this.inputDom = document.getElementById(this.inputTextId);
        var containerdiv = $("<div id='" + othis.ajaxId + "'></div>");
        var divcss = {
            position: "absolute",
            padding: "0px",
            border: "1pt solid #000000",
            display: "none",
            "background": "#FFFFFF",
            "z-index": "10000",
            "overflow": "hidden"
        };
        containerdiv.css(divcss);
        containerdiv.width(othis.Width);
        containerdiv.height(othis.Height);
        //containerdiv.addClass(othis.containerClassName);
        var pos = $("#" + othis.inputTextId).offset();
        var docDoH = $(document).height();
        if (pos.top + othis.Height > docDoH - 30 && docDoH > othis.Height * 2) {
            containerdiv.css("left", pos.left);
            containerdiv.css("top", pos.top - othis.Height - 3);
        }
        else {
            containerdiv.css("left", pos.left);
            containerdiv.css("top", pos.top + $("#" + othis.inputTextId).height() + 3);
        }
        $(document.body).append(containerdiv);
        this.divDom = document.getElementById(this.ajaxId);
        //创建divDomIn
        var incss = {
            "display": "block",
            "border-style": "none",
            "border-width": "0px",
            "overflow": "scroll"
        };
        var $divDomIn = $("<div></div>");
        this.divDomIn = $divDomIn[0];
        $divDomIn.addClass(incss);
        $divDomIn.height(othis.Height);
        $divDomIn.width(othis.Width);
        containerdiv.append(this.divDomIn);
        $divDomIn.addClass(othis.containerClassName);
        othis.addEvent();

        if ($.browser.msie && /msie 6\.0/i.test(navigator.userAgent)) {
            //            var ifcss = {
            //                "position": "absolute",
            //                "z-index": "-1",
            //                "height": "100%",
            //                "width": "100%",
            //                "display": "block",
            //                "border-style": "none",
            //                "border-width": "0px"
            //            };
            //            var ifreamhid = $("<iframe>");
            //            ifreamhid.css(ifcss);
            //            ifreamhid.width(othis.Width);
            //            ifreamhid.height(othis.Height);
            //            $(othis.divDom).append(ifreamhid);
            //            othis.hidiframeHTML = othis.divDom.innerHTML;
            othis.getHidIframe();
        }
    };
    this.isHasIframe = false;
    this.getHidIframe = function () {
        if (othis.isHasIframe) {
            return;
        }
        var ifcss = {
            "position": "absolute",
            "z-index": "-1",
            "height": "100%",
            "width": "100%",
            "display": "block",
            "border-style": "none",
            "border-width": "0px"
        };
        var ifreamhid = $("<iframe>");
        ifreamhid.css(ifcss);
        //ifreamhid.width(othis.Width);
        //ifreamhid.height(othis.Height);

        $(othis.divDom).prepend(ifreamhid);
        othis.isHasIframe = true;
        //othis.hidiframeHTML = othis.divDom.innerHTML;
    };
    this.getTable = function () {
        return $(othis.divDomIn).children()[0];
    };
    this.timeHandle = null;
    this.isNeedHiden = false;
    this.isMouseOut = true;
    this.HidenAuto = function () {
        if (!othis.IsShow) {
            return;
        }
        if (othis.AutoHiden) {
            if (othis.isNeedHiden && othis.isMouseOut) {
                //clearInterval(othis.timeHandle);
                othis.hiden();
            } else {
                othis.timeHandle = setTimeout(othis.HidenAuto, 3000);
            }
        }
    };

    //添加输入框的筛选时间 占用时间 onkeyUp onkeydown
    this.addEvent = function () {
        $(othis.inputDom).blur(function (e) {
            othis.isNeedHiden = true;
            setTimeout(othis.HidenAuto, 3000);
        }); //end blur
        $(othis.inputDom).keydown(function (e) {
            othis.isNeedHiden = false;
            othis.isMouseOut = true;
            if (!othis.Enabled) {
                return;
            }
            var currentAjaxStr = othis.inputDom.value;
            var keyCode = window.event.keyCode;

            if (keyCode == 27) {
                othis.hiden();
            }
            if (currentAjaxStr.length == 0) {
                if (keyCode == 40) {
                    if (othis.isSelecting) {
                        othis.chooseDown();
                        return;
                    }
                    //                    othis.isSelecting = true;
                    //                    othis.sendRequst();
                }
                if (keyCode == 38 && othis.isSelecting) {
                    othis.chooseUp();
                    return;
                }
                if (keyCode == 13) {
                    if (keyCode == 13 && othis.IsShow) {
                        othis.doGetResult();
                    }
                }
            }
            if (keyCode == 38) {
                othis.chooseUp();
                return;
            }
            if (keyCode == 40) {
                othis.chooseDown();
                return;
            }
        });
        $(othis.inputDom).keyup(function () {
            //添加文本输入框keyUp事件
            if (!othis.Enabled) {
                if (othis.DoFunEnterWhenDisable) {
                    othis.DoFunEnterWhenDisable();
                }
                return;
            }
            var currentAjaxStr = othis.inputDom.value;
            var keyCode = window.event.keyCode;

            if (currentAjaxStr.length == 0) {
                if (keyCode == 40) {
                    if (othis.isSelecting) {
                        //othis.chooseDown();
                        return;
                    }
                    //othis.isSelecting = true;
                    //othis.sendRequst();
                }
                if (keyCode == 38 && othis.isSelecting) {
                    othis.chooseUp();
                    return;
                }
                if (keyCode == 13 && othis.isSelecting) {
                    othis.doGetResult();
                    return;
                }
                if (keyCode == 8) {

                    othis.hiden();
                }
                return;
            }
            //if (keyCode == 8 && ZYCOMMSelectIsBackspaceNotShow){
            //    othis.hiden();
            //    return;
            //}
            othis.isSelecting = false;
            if (keyCode == 38) {
                //                othis.chooseUp();
                return;
            }
            if (keyCode == 40) {
                //                othis.chooseDown();
                return;
            }
            if (keyCode == 13) {
                othis.doGetResult();
                return;
            }

            if (!othis.checkStrIsAfterStr(othis.lastInputId, currentAjaxStr)) {
                othis.sendRequst();
                othis.lastInputId = currentAjaxStr;
                return;
            }
            othis.lastInputId = currentAjaxStr;
            if (!othis.isOnSelectSelf) {
                if (currentAjaxStr.length >= othis.limitLen) {
                    othis.sendRequst();
                    return;
                }
            }
            if (currentAjaxStr.length >= othis.limitLen && currentAjaxStr.length < othis.HowLenStrWhereSelectByClient) {
                //需要回调
                othis.sendRequst();
                return;
            }
            if (currentAjaxStr.length >= othis.limitLen && currentAjaxStr.length >= othis.HowLenStrWhereSelectByClient) {
                //自行判断
                othis.show();
                //var table = $("#" + othis.ajaxId + " > table")[0];
                var table = othis.getTable();
                var j = 0;
                if (!table) {
                    while (j++ < 20000) {
                        if (table) {
                            break;
                        }
                    }
                    // table = $("#" + othis.ajaxId + " > table")[0];
                    table = othis.getTable();
                    if (!table) {
                        return;
                    }
                }
                var trs = table.rows;
                var len = trs.length;
                var trstrs = [];
                eval("var reg = new RegExp(/^" + currentAjaxStr + "/i);");
                trstrs.push("<table cellpadding='0' cellspacing='0'>");
                trstrs.push(trs[0].outerHTML);
                if (othis.isNotOrderBuInput) {
                    for (var i = 1, tr; tr = trs[i]; i++) {
                        if (reg.test(tr.cells[othis.InputCodeShowCol].innerText)) {
                            trstrs.push(tr.outerHTML);
                        }
                    }
                }
                else {
                    for (var i = 1, tr; tr = trs[i]; i++) {
                        if (reg.test(tr.cells[othis.InputCodeShowCol].innerText)) {
                            for (var j = i, trtmp; trtmp = trs[j]; j++) {
                                if (!reg.test(trtmp.cells[othis.InputCodeShowCol].innerText)) {
                                    break;
                                }
                                trstrs.push(trtmp.outerHTML);
                            }
                            break;
                        }
                    }
                }

                trstrs.push("</table>");
                $(othis.divDomIn).html(trstrs.join(""));
                othis.initChoose();
            }
        });
        $(othis.divDomIn).click(function () {
            othis.isMouseOut = false;
            window.event.cancelBubble = true;
            var domtd = event.srcElement;
            if ((domtd.tagName + "").toUpperCase() == "TD") {
                othis.makeTrSelected($(domtd).parent()[0]);
            }
        });
        $(othis.divDomIn).dblclick(function () {
            othis.isMouseOut = false;
            window.event.cancelBubble = true;
            var domtd = event.srcElement;
            if ((domtd.tagName + "").toUpperCase() == "TD") {
                othis.makeTrSelected($(domtd).parent()[0]);
                othis.doGetResult();
            }
        });
        $(othis.divDomIn).mouseover(function () {
            othis.isMouseOut = false;
        });
        $(othis.divDomIn).mousemove(function () {
            othis.isMouseOut = false;
        });
        $(othis.divDomIn).mouseout(function () {
            othis.isMouseOut = true;
        });
        $(othis.divDomIn).keyup(function () {
            if (window.event.keyCode == 13) {
                othis.doGetResult();
            }
        });
        $(othis.divDomIn).mousedown(function (e) {
            zy$.CancerBubble(e);
        });
        $(othis.divDomIn).mouseup(function (e) {
            zy$.CancerBubble(e);
        });
        $(othis.divDom).click(function (e) {
            zy$.CancerBubble(e);
        });
        $(othis.divDom).mouseup(function (e) {
            zy$.CancerBubble(e);
        });
        $(othis.divDom).mousedown(function (e) {
            zy$.CancerBubble(e);
        });
    };
    
    //发送请求
    this.sendRequst = function () {
    	/*
        if (true) {   //////////////////////////////////////测试
//            var testjosn = '{                                                                         '
//+ '                                "HEAD": ['
//+ '		                                      {"COL":"内码","ISSHOW":"FALSE","ISID":"TRUE"},                   '
//+ '                                           {"COL":"名称","ISSHOW":"TRUE"},'
//+ '                                           {"COL":"年龄","ISSHOW":"TRUE"},'
//+ '                                           {"COL":"性别","ISSHOW":"TRUE"}'
//+ '                                        ],'
//+ '                                "ROW":[  ['
//+ '		                                        {"COL":"123"},'
//+ '                                             {"COL":"小明","ISSHOW":"TRUE"},'
//+ '                                             {"COL":"20","ISSHOW":"TRUE"},'
//+ '                                             {"COL":"男","ISSHOW":"TRUE"},'
//+ '		                                        {"ColorCol":"RED"}'
//+ '                                         ],'
//+ '                                         ['
//+ '		                                        {"COL":"234"},'
//+ '                                             {"COL":"大明","ISSHOW":"TRUE"},'
//+ '                                             {"COL":"30","ISSHOW":"TRUE"},'
//+ '                                             {"COL":"男","ISSHOW":"TRUE"}'
//+ '                                         ]'
//+ '                                      ]'
//+ '                            }';


            var testjosn = '{                                                                         '
+ '                                "HEAD": ['
+ '		                                      {"COLID":"PID","COL":"内码","ISSHOW":"FALSE","ISID":"TRUE"},                   '
+ '                                           {"COLID":"PNAME","COL":"名称","ISSHOW":"TRUE"},'
+ '                                           {"COLID":"PAGE","COL":"年龄","ISSHOW":"TRUE"},'
+ '                                           {"COLID":"PSEX","COL":"性别","ISSHOW":"TRUE"}'
+ '                                        ],'
+ '                                "ROW":[  {'
+ '		                                        "PID":"123",'
+ '                                             "PNAME":"小明",'
+ '                                             "PAGE":"20",'
+ '                                             "PSEX":"男",'
+ '		                                        "ColorCol":"RED"'
+ '                                         },'
+ '                                         {'
+ '		                                        "PID":"234",'
+ '                                             "PNAME":"大明",'
+ '                                             "PAGE":"30",'
+ '                                             "PSEX":"男"'
+ '                                         }'
+ '                                      ]'
+ '                            }';



            othis.divDomIn.innerHTML = othis.createTableStr(testjosn);
            othis.initChoose();
            return;
        }

*/
        var paramJson = eval('(' + '{"inputstr":"' + othis.inputDom.value + '","Addtion":"' + othis.Addtion + '"}' + ')');

        $.ajax({
            type: "post",
            dataType: "json",
            url: othis.pageurl,
            data: paramJson,
            success: function (data) {
                othis.divDomIn.innerHTML = othis.createTableStr(data);
                othis.initChoose();
            },
            error: function () {
            }
        });
    };

    //把后台返回的json数据生成表格HTML字符串
    this.createTableStr = function (json) {
        //json = eval('(' + json + ')');

        var idIndex = "";
        var strTableHtml = "<table cellpadding='0' cellspacing='0'>";
        strTableHtml += "<tr>";
        for (var i in json.HEAD) {
            if (json.HEAD[i].ISID == "TRUE") {
                idIndex = json.HEAD[i].COLID;
            }
            strTableHtml += "<td nowrap class='ajaxSelectTd" + i + "' " + ((json.HEAD[i].ISSHOW == "FALSE") ? "style='width: 1px; display:none;'" : "") + " >" + json.HEAD[i].COL + "</td>";
        }
        strTableHtml += "</tr>";

        for (var i in json.ROW) {
            var curRowId = "";
            if (idIndex != '') {
                curRowId = eval('json.ROW[' + i + '].' + idIndex);
            }
            strTableHtml += "<tr title='" + curRowId + "'";

            if (json.ROW[i].ColorCol != "") {
                strTableHtml += "style='color:" + json.ROW[i].ColorCol + ";'";
            }

            strTableHtml += ">";

            for (var j in json.HEAD) {

                strTableHtml += "<td nowrap class='ajaxSelectTd" + j + "' " + ((json.HEAD[j].ISSHOW == "FALSE") ? "style='width: 1px;display:none;'" : "") + " >";

                strTableHtml += eval('json.ROW[' + i + '].' + json.HEAD[j].COLID);
                strTableHtml += "</td>";
            }
            strTableHtml += "</tr>";
        }

        strTableHtml += "</table>";
        return strTableHtml;
    };





    //检查strbefore 是不是被包含在strAfter中
    this.checkStrIsAfterStr = function (strBefore, strAfter) {
        if (strAfter.indexOf(strBefore) != 0) {
            return false;
        }
        return true;
    };
    //初始化选择
    this.initChoose = function () {
        othis.show();
        //var table = $("#" + othis.ajaxId + " > table")[0];
        var table = othis.getTable();
        var trs = table.rows;
        var len = trs.length;
        if (len < 2) {
            othis.SelectTr = null;
            if (this.IsEmptyHiden) {
                othis.hiden();
            }
        }
        else {
            $(trs[0]).addClass(othis.tableHeadClass);
            othis.headtr = trs[0];
            othis.SelectTr = trs[1];
            $(othis.SelectTr).addClass(othis.selectTrClassName);
        }
    };
    //向上查找
    this.chooseUp = function () {
        if (othis.SelectTr == null) {
            return;
        }
        //var table = $("#" + othis.ajaxId + " > table")[0];
        var table = othis.getTable();
        var trs = table.rows;
        var len = trs.length;
        var currentIndex = othis.SelectTr.rowIndex;
        var nextIndex = currentIndex - 1;
        if (nextIndex < 1) {
            nextIndex = len - 1;
        }
        var tr = trs[nextIndex];
        othis.makeTrSelected(tr, true);
    };

    //向下查找
    this.chooseDown = function () {
        if (othis.SelectTr == null) {
            return;
        }
        //var table = $("#" + othis.ajaxId + " > table")[0];
        var table = othis.getTable();
        var trs = table.rows;
        var len = trs.length;
        var currentIndex = othis.SelectTr.rowIndex;
        var nextIndex = currentIndex + 1;
        if (nextIndex >= len) {
            nextIndex = 1;
        }
        var tr = trs[nextIndex];
        othis.makeTrSelected(tr, true);
    };
    //获取当前的饿选取结果
    this.doGetResult = function () {
        if (!othis.IsShow) {
            return;
        }
        if (othis.SelectTr == null) {
            othis.doSelectFun({
                id: '',
                content: [],
                showStatus: [],
                contentJson: [],
                contenttitle: []
            });
            if (othis.DoFunEnter) {
                othis.DoFunEnter();
            }
            return;
        }
        var idvalue = othis.SelectTr.title;
        var contentarr = [];
        var showStatus = [];
        var cells = othis.SelectTr.childNodes;
        var headcells = othis.headtr.childNodes;

        var contentjson = [];
        var contenttitles = [];
        for (var i = 0, cell; cell = cells[i]; i++) {
            //获取每一列的值，显示状态
            contentarr.push(cell.innerText);
            contentjson[headcells[i].innerText] = cell.innerText;
            contenttitles[headcells[i].title] = cell.innerText;
            if (cell.style.display == "none") {
                showStatus.push(false);
            } else {
                showStatus.push(true);
            }
        }
        othis.doSelectFun({
            id: idvalue,
            content: contentarr,
            showStatus: showStatus,
            contentJson: contentjson
        });
        othis.hiden();
        if (othis.DoFunEnter) {
            othis.DoFunEnter();
        }
    };
    //选择某一行
    this.makeTrSelected = function (trDom, isFixHeight) {
        if (trDom) {
            if (trDom.rowIndex < 1) {
                othis.SelectTr = null;
                return;
            }
        }
        if (othis.SelectTr != null) {
            $(othis.SelectTr).removeClass(othis.selectTrClassName);
        }
        othis.SelectTr = trDom;
        var selecttr = $(othis.SelectTr);
        selecttr.addClass(othis.selectTrClassName);
        if (isFixHeight) {
            var height = othis.SelectTr.offsetTop;
            height += selecttr.height();
            if (height > othis.Height * 0.9) {
                $(othis.divDomIn).scrollTop(height - othis.Height * 0.9);
            }
            else {
                $(othis.divDomIn).scrollTop(0);
            }
            //滑动条：
        }
        //下面是调整高度
    };
    //隐藏
    this.hiden = function () {
        $(othis.divDom)[0].style.display = "none";
        othis.IsShow = false;
    };
    //显示
    this.show = function () {
        if (othis.csEveryTime) {
            var containerdiv = $(othis.divDom);
            var pos = $("#" + othis.inputTextId).offset();

            var docDoH = $(document).height();
            if (pos.top + othis.Height > docDoH - 30 && docDoH > othis.Height * 2) {
                containerdiv.css("left", pos.left);
                containerdiv.css("top", pos.top - othis.Height - 3);
            }
            else {
                containerdiv.css("left", pos.left);
                containerdiv.css("top", pos.top + $("#" + othis.inputTextId).height() + 3);
            }
        }
        $(othis.divDom)[0].style.display = "block";
        othis.IsShow = true;
    };
    this.init();
}

//#endregion