<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Kettle脚本运行控制台</title>
		<link href="<%=request.getContextPath()%>/static/weblib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" />  
	    <script src="<%=request.getContextPath()%>/static/weblib/jquery/jquery-1.10.2.min.js" type="text/javascript"></script>    
	    <script src="<%=request.getContextPath()%>/static/weblib/ligerUI/js/ligerui.all.js" type="text/javascript"></script>
	    <script type="text/javascript">
		    $(function ()
	        {
	            $("#maingrid4").ligerGrid({
	                columns: [
	                  {display: '序号', name: 'id', align: 'left', width: 120 } ,
	                  { display: '作业名称', name: 'jobName', minWidth: 60 },
	                  { display: '运行状态', name: 'runningStatus' }
	                ], 
	                width: '100%', height: '99%', checkbox: true
	            }); 
	        });
	    </script>
	</head>
	<body>
		<div id="maingrid4" style="margin: 0; padding: 0"></div> 
	</body>
</html>