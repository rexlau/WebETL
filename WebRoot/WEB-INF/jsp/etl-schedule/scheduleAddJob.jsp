<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Kettle脚本运行控制台</title>
	    <link href="<%=request.getContextPath()%>/static/weblib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" />
	    <link href="<%=request.getContextPath()%>/static/weblib/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
	    <link href="<%=request.getContextPath()%>/static/weblib/ligerUI/skins/Gray/css/all.css" rel="stylesheet" type="text/css" />
	    <script src="<%=request.getContextPath()%>/static/weblib//jquery/jquery-1.10.2.min.js" type="text/javascript"></script> 
	    <script src="<%=request.getContextPath()%>/static/weblib/ligerUI/js/core/base.js" type="text/javascript"></script> 
	    <script src="<%=request.getContextPath()%>/static/weblib/ligerUI/js/plugins/ligerGrid.js" type="text/javascript"></script>
	    <script src="<%=request.getContextPath()%>/static/weblib/ligerUI/js/plugins/ligerToolBar.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/static/weblib/ligerUI/js/plugins/ligerResizable.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/static/weblib/ligerUI/js/plugins/ligerCheckBox.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/static/weblib/layer1.8/layer.min.js" type="text/javascript"></script>
	        
	    <script type="text/javascript">
	    	var grid;
	        $(function ()
	        {
	        	grid = $("#maingrid").ligerGrid({
	                height:'100%',
	                columns: [
		                { display: '作业ID', name: 'idJob', align: 'left', width: 100, minWidth: 60 },
		                { display: '作业名称', name: 'name', minWidth: 120 },
		                { display: '作业路径', name: 'absolutePath', minWidth: 140 },
		                { display: '修改时间', name: 'modifiedDate' },
		                { display: '操作', name: 'operator',
		                	render: function (row)
		    				{
		    					var html = '<a href="void:javascript(0)" onclick=addSchedulerJob("'+row['__id']+'")>添加</a>';
		    				    return html;
		    				}	
		                }
	                ], 
	                url:'<%=request.getContextPath()%>/etlSchedule/loadRepositoryJobs.action',
	                usePager:false,
	            });
	             
	            $("#pageloading").hide();
	            
	        });
		    
		  //添加作业任务
		    function addSchedulerJob(obj){
		    	var row = grid.getRow(obj);
		    	delete row.modifiedDate;
		    	delete row.createdDate;
		    	var url = '<%=request.getContextPath()%>/etlSchedule/addRepositoryJobs.action';
		    	
		    	ajaxJosnRequest(url,JSON.stringify(row),function(datas){
		    		alert(datas);
		    	})
		    	
		    }
		    function ajaxJosnRequest(url,params,successFn){
		    	$.ajax({
		    		type:'post',
					url: url,
					contentType : "application/json;charset=UTF-8",
					dataType: 'json',
					data:params,
					success: function(datas){
						successFn(datas);
					}
		    })
		   }
	    </script>
	    <style>
	    	.l-job-running{
	    		background: #AFE711;
	    	}
	    </style>
	</head>
	
	<body style="overflow-x:hidden; padding:2px;">
<div class="l-loading" style="display:block" id="pageloading"></div>
 <a class="l-button" style="width:120px;float:left; margin-left:10px; display:none;" onclick="deleteRow()">删除选择的行</a>

 
 <div class="l-clear"></div>

    <div id="maingrid"></div>
   
  <div style="display:none;">
  
</div>
 
</body>
</html>