<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Kettle运行日志查询</title>
		<link href="<%=request.getContextPath()%>/static/weblib/jquery/treetable/css/jquery.treetable.css" rel="stylesheet" type="text/css" />
		<link href="<%=request.getContextPath()%>/static/weblib/jquery/treetable/css/jquery.treetable.theme.default.css" rel="stylesheet" type="text/css" />
	    <script src="<%=request.getContextPath()%>/static/weblib/jquery/jquery-1.10.2.min.js" type="text/javascript"></script>
	    <script src="<%=request.getContextPath()%>/static/weblib/jquery/treetable/jquery.treetable.js" type="text/javascript"></script> 
	    <script type="text/javascript">
	    	$(function (){
	    		$("#example-advanced").treetable({ expandable: true });
            });
	    </script>
	</head>
	
	<body >
		<c:choose>
        	<c:when test="${not empty jobMetricList}">
		 		<table id="example-advanced">
			        <caption>
			          <a href="#" onclick="jQuery('#example-advanced').treetable('expandAll'); return false;">Expand all</a>
			          <a href="#" onclick="jQuery('#example-advanced').treetable('collapseAll'); return false;">Collapse all</a>
			        </caption>
			        <thead>
			          <tr>
			            <th width="200px" nowrap>Name</th>
			            <th width="200px" nowrap>Kind</th>
			            <th width="200px" nowrap>Size</th>
			            <th width="300px" nowrap>Size</th>
			            <th width="200px" nowrap>Name</th>
			          </tr>
			        </thead>
		        	<tbody>
			        	<c:forEach var="item" items="${jobMetricList}" varStatus="status">
			        	<tr data-tt-id='${item.channelId}' data-tt-parent-id='${item.parentChannelId }'>
			       			<td>${item.jobjobentry}</td>
			       			<td>${item.comments}</td>
			       			<td>${item.result}</td>
			       			<td>${item.logDate}</td>
			       			<td>${item.reason}</td>
			      		</tr>
			        	</c:forEach>
		     	</tbody>
	      </table>
	    </c:when>
        <c:otherwise>
			${NO_DATA}
		</c:otherwise>
	</c:choose>
	</body>
</html>