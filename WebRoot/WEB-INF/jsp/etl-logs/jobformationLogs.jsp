<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Kettle运行日志查询</title>
		<link href="<%=request.getContextPath()%>/static/css/style.css" rel="stylesheet" type="text/css" />
	    <link href="<%=request.getContextPath()%>/static/weblib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" />
	    <link href="<%=request.getContextPath()%>/static/weblib/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
	    <link href="<%=request.getContextPath()%>/static/weblib/ligerUI/skins/Gray/css/all.css" rel="stylesheet" type="text/css" />
	    <script src="<%=request.getContextPath()%>/static/weblib//jquery/jquery-1.10.2.min.js" type="text/javascript"></script> 
	    <script src="<%=request.getContextPath()%>/static/weblib/ligerUI/js/core/base.js" type="text/javascript"></script> 
		<script src="<%=request.getContextPath()%>/static/weblib/ligerUI/js/plugins/ligerLayout.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/static/weblib/ligerUI/js/plugins/ligerTab.js" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/static/weblib/ligerUI/js/plugins/ligerDrag.js" type="text/javascript"></script>
	    <script src="<%=request.getContextPath()%>/static/weblib/ligerUI/js/plugins/ligerGrid.js" type="text/javascript"></script>
	    <script type="text/javascript">
	    	var url1 = '<%=request.getContextPath()%>/etlLogs/loadJobformationMetric.action';
	    	var url2 = '<%=request.getContextPath()%>/etlLogs/loadJobformationLogText.action';
	    	$(function (){
	    		$("#layout1").ligerLayout({ leftWidth: 200});
	    		var height = $(".l-layout-center").height();
				$("#navtab1").ligerTab({
					height:height
				});
	            $("#navtab2").ligerTab({
					height:height
				});
	            
	            $(".menuson").find('a').first().trigger("click");
            });
	    	
	    	function refreshLogTab(idJob){
	    		$('#logMetric').attr('src', url1+'?jobChannelId='+idJob);
	    		$('#logDetail').attr('src', url2+'?jobChannelId='+idJob);
	    	}
	    </script>
	</head>
	
	<body style="overflow-x:hidden; padding:2px;">
		<div id="layout1">
            <div position="left" title="记录日志日期">
			<ul class="menuson show">
				<c:choose>
	        		<c:when test="${not empty jobList}">
		        		<c:forEach var="item" items="${jobList}" varStatus="status">
		        			<li>
		        				<cite></cite>
		        				<a onclick="refreshLogTab('${item.channelId}')">
		        						${item.logdate}
		        				</a>
		        				<i></i>
		        			</li>
			        	</c:forEach>
			        </c:when>
			   	</c:choose>
			</ul>
		</div>
            <div position="center">
	            <div id="navtab1" style="overflow:hidden; border:1px solid #A3C0E8; ">
		            <div tabid="logMetric" title="作业度量" lselected="true"  style="height:100%" >
		            	<iframe frameborder="0" id="logMetric" scrolling="auto" src="<%=request.getContextPath()%>/etlLogs/loadJobformationMetric.action"></iframe>
		            </div>
		            <div tabid="logDetail" title="详细日志" lselected="true"  style="height:300px" >
		            	<iframe frameborder="0" id="logDetail" scrolling="auto" src="<%=request.getContextPath()%>/etlLogs/loadJobformationLogText.action"></iframe>
		            </div>
		            
	            </div>
            </div>  
        </div>
 
</body>
</html>