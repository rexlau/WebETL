package com.msunsoft.test.etl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.msunsoft.main.etl.entity.ETL_LOG_JOBS;
import com.msunsoft.main.etl.service.ScriptLogsService;

/**
 * 测试job日志相关sercice
 * @author Administrator
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:resources/spring/applicationContext.xml")
public class TestJobformationLogs {

	@Resource
	private ScriptLogsService scriptLogsService;
	
	
	@Test
	public void getJobBaseLogs(){
		Map<String, String> etlLogJobs = new  HashMap<String, String>();
		etlLogJobs.put("jobname", "job1");
		List<ETL_LOG_JOBS> jobLogsList = scriptLogsService.selectBaseETLJobLogs(etlLogJobs);
		for(ETL_LOG_JOBS jobLog : jobLogsList)
			System.out.println(jobLog.toString());
	}
	
	@Test
	public void getJobBlobLogs(){
		Map<String, String> etlLogJobs = new  HashMap<String, String>();
		etlLogJobs.put("channelId", "b775233c-ed1b-438d-8960-b737e9d51960");
		ETL_LOG_JOBS jobLogs = scriptLogsService.selectETLJobLogsBlob(etlLogJobs);
		System.out.println(jobLogs.toString());
	}
}
