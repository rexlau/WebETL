package com.msunsoft.test.etl;

import org.junit.Test;
import org.pentaho.di.core.KettleEnvironment;
import org.pentaho.di.core.database.DatabaseMeta;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.job.Job;
import org.pentaho.di.job.JobMeta;
import org.pentaho.di.repository.LongObjectId;
import org.pentaho.di.repository.ObjectId;
import org.pentaho.di.repository.kdr.KettleDatabaseRepository;
import org.pentaho.di.repository.kdr.KettleDatabaseRepositoryMeta;

public class TestKettleTransformationRun {
	
	@Test
	public void KettleTransformationRunningFromRep(){
		ObjectId id = new LongObjectId(1);
		try {
            KettleEnvironment.init();
            //数据库连接对象
            DatabaseMeta dataMeta = new DatabaseMeta("local","Oracle","Native(JDBC)","112.230.253.51","rmdb","15211","etl","etl");
            //资源库元对象
            KettleDatabaseRepositoryMeta repInfo = new KettleDatabaseRepositoryMeta();
            repInfo.setConnection(dataMeta);
            //资源库
            KettleDatabaseRepository rep = new KettleDatabaseRepository();
            rep.init(repInfo);
            rep.connect("admin", "admin");
            rep.disconnect();
            System.out.println("成功连接资源库");
            JobMeta jobMeta = rep.loadJob(id,null);
            Job job = new Job(rep, jobMeta);
            job.start();
            job.waitUntilFinished();
            if(job.getErrors()>0) 
            {                   
                System.err.println("job run Failure!"); 
            } 
            else 
            { 
                System.out.println("job run successfully!"); 
            } 
        } catch (KettleException e) {
            e.printStackTrace();
        }
	}
}
