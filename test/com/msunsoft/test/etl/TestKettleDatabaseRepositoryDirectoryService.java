package com.msunsoft.test.etl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.msunsoft.main.etl.service.KettleDirectoryService;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:resources/spring/applicationContext.xml")
public class TestKettleDatabaseRepositoryDirectoryService {

	@Autowired
	KettleDirectoryService kettleDirectoryService;
	
	
	@Test
	public void getAbsolutePath(){
		System.out.println(kettleDirectoryService.getAbsolutePath(17));
	}
	
}
