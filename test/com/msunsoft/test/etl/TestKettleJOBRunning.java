package com.msunsoft.test.etl;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.msunsoft.main.etl.engine.KettleEnvironmentRunEngineService;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:resources/spring/applicationContext.xml")
public class TestKettleJOBRunning {

	@Resource
	private KettleEnvironmentRunEngineService runJobContainer;
	
	
	@Test
	public void getAbsolutePath(){
		runJobContainer.runJobformation(1L);
	}
	
}
