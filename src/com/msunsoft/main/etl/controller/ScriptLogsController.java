package com.msunsoft.main.etl.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.msunsoft.main.etl.entity.ETL_LOG_JOBS;
import com.msunsoft.main.etl.entity.ETL_LOG_JOB_METRIC;
import com.msunsoft.main.etl.service.ScriptLogsService;
import com.msunsoft.main.utils.Const;

/**
 * 作业运行日志
 * @author Administrator
 *
 */
@Controller
@RequestMapping(value="/etlLogs")
public class ScriptLogsController {
	
	@Resource
	private ScriptLogsService scriptLogsService;
	
	@RequestMapping(value="/jobformationLogsView", produces = { "text/json;charset=utf-8" })
	public ModelAndView scheduleControllerView(HttpServletRequest request){
		ModelAndView view = new ModelAndView();
		String jobName = request.getParameter("jobName");
		if(jobName != null && jobName.length()>0){
			Map<String, String> etlLogJobs = new HashMap<String, String>();
			etlLogJobs.put("jobname", jobName);
			List<ETL_LOG_JOBS> jobList = scriptLogsService.selectBaseETLJobLogs(etlLogJobs);
			view.setViewName("etl-logs/jobformationLogs");
			view.addObject("jobList",jobList);
		}
		return view;
	}
	
	@RequestMapping(value="/loadJobformationMetric")
	public ModelAndView loadJobformationMetric(HttpServletRequest request){
		ModelAndView view = new ModelAndView();
		String jobChannelId = request.getParameter("jobChannelId");
		if(jobChannelId != null && jobChannelId.length()>0){
			Map<String, String> etlLogJobs = new HashMap<String, String>();
			etlLogJobs.put("parentChannelId", jobChannelId);
			List<ETL_LOG_JOB_METRIC> jobMetricList = scriptLogsService.selectETLJobMetrics(etlLogJobs);
			view.addObject("jobMetricList",jobMetricList);
		}else{
			view.addObject(Const.HTTPRESPONSE.NO_DATA.id,"未查询到相关日志！");
		}
		view.setViewName("etl-logs/jobformationLogsMetric");
		return view;
	}
	
	@RequestMapping(value="/loadJobformationLogText")
	public ModelAndView loadJobformationLogDetail(HttpServletRequest request){
		ModelAndView view = new ModelAndView();
		String jobChannelId = request.getParameter("jobChannelId");
		if(jobChannelId != null && jobChannelId.length()>0){
			Map<String, String> etlLogJobs = new HashMap<String, String>();
			etlLogJobs.put("channelId", jobChannelId);
			ETL_LOG_JOBS jobLogText = scriptLogsService.selectETLJobLogsBlob(etlLogJobs);
			view.addObject("jobLogText",jobLogText);
		}else{
			view.addObject(Const.HTTPRESPONSE.NO_DATA.id,"未查询到相关日志！");
		}
		view.setViewName("etl-logs/jobformationLogsText");
		return view;
	}
	
	@RequestMapping(value="/loadJobformationBaseLog")
	public @ResponseBody String loadJobformationBaseLog(@RequestBody Map etlLogJobs){
		List<ETL_LOG_JOBS> jobList = scriptLogsService.selectBaseETLJobLogs(etlLogJobs);
		Map reponse = new HashMap();
		reponse.put("Rows", jobList);
		ObjectMapper objectWraper = new ObjectMapper();
		try {
			return objectWraper.writeValueAsString(reponse);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}

