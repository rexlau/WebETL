package com.msunsoft.main.etl.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.msunsoft.main.etl.service.KettleJobService;

/**
 * 作业运行控制
 * @author Administrator
 *
 */
@Controller
@RequestMapping(value="/")
public class AjaxSelectDemoController {
	
	@Resource
	private KettleJobService kettleJobService;
	
	
	@RequestMapping(value="/ajaxSelect")
	public @ResponseBody String ajaxSelect(){
		Map reponseBody = new HashMap();//返回响应的
		
		/*********************Head部分拼接************************/
		List<Map> headerDataList = new ArrayList<Map>();
		Map<String, String> headerdata = new HashMap<String, String>();
		headerdata.put("COLID","PID");
		headerdata.put("COL", "内码");
		headerdata.put("ISSHOW", "FALSE");
		headerdata.put("ISID", "TRUE");
		headerDataList.add(headerdata);
		
		headerdata = new HashMap();
		headerdata.put("COLID","PNAME");
		headerdata.put("COL", "名称");
		headerdata.put("ISSHOW", "TRUE");
		headerDataList.add(headerdata);
		
		headerdata = new HashMap();
		headerdata.put("COLID","PAGE");
		headerdata.put("COL", "年龄");
		headerdata.put("ISSHOW", "TRUE");
		headerDataList.add(headerdata);
		
		headerdata = new HashMap();
		headerdata.put("COLID","PSEX");
		headerdata.put("COL", "性别");
		headerdata.put("ISSHOW", "TRUE");
		headerDataList.add(headerdata);
		
		reponseBody.put("HEAD", headerDataList);
		/*********************Head部分拼接结束************************/
		List<Map> rowsDataList = new ArrayList();
		
		Map<String, String> rowData = new HashMap<String, String>();
		rowData.put("PID", "123");
		rowData.put("PNAME", "小明");
		rowData.put("PAGE", "20");
		rowData.put("PSEX", "男");
		rowsDataList.add(rowData);
		
		rowData = new HashMap<String, String>();
		rowData.put("PID", "234");
		rowData.put("PNAME", "大明");
		rowData.put("PAGE", "30");
		rowData.put("PSEX", "男");
		rowsDataList.add(rowData);
		
		reponseBody.put("ROW", rowsDataList);
		
		
		ObjectMapper objMapper = new ObjectMapper();
		try {
			return objMapper.writeValueAsString(reponseBody);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	public static void main(String[] args) {
		AjaxSelectDemoController a = new AjaxSelectDemoController();
		System.err.println(a.ajaxSelect());
	}
}
