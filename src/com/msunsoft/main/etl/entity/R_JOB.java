package com.msunsoft.main.etl.entity;

import java.util.Date;

public class R_JOB {
    private int idJob;

    private int idDirectory;

    private String name;

    private String jobVersion;

    private int jobStatus;

    private int idDatabaseLog;

    private String tableNameLog;

    private String createdUser;

    private Date createdDate;

    private String modifiedUser;

    private Date modifiedDate;

    private String useBatchId;

    private String passBatchId;

    private String useLogfield;

    private String sharedFile;
    
    /////////////////额外增加的属性
    private String absolutePath;
    
    public String getAbsolutePath() {
		return absolutePath;
	}

	public void setAbsolutePath(String absolutePath) {
		this.absolutePath = absolutePath;
	}
	/////////////////////

	public int getIdJob() {
        return idJob;
    }

    public void setIdJob(int idJob) {
        this.idJob = idJob;
    }

    public int getIdDirectory() {
        return idDirectory;
    }

    public void setIdDirectory(int idDirectory) {
        this.idDirectory = idDirectory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getJobVersion() {
        return jobVersion;
    }

    public void setJobVersion(String jobVersion) {
        this.jobVersion = jobVersion == null ? null : jobVersion.trim();
    }

    public int getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(int jobStatus) {
        this.jobStatus = jobStatus;
    }

    public int getIdDatabaseLog() {
        return idDatabaseLog;
    }

    public void setIdDatabaseLog(int idDatabaseLog) {
        this.idDatabaseLog = idDatabaseLog;
    }

    public String getTableNameLog() {
        return tableNameLog;
    }

    public void setTableNameLog(String tableNameLog) {
        this.tableNameLog = tableNameLog == null ? null : tableNameLog.trim();
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser == null ? null : createdUser.trim();
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedUser() {
        return modifiedUser;
    }

    public void setModifiedUser(String modifiedUser) {
        this.modifiedUser = modifiedUser == null ? null : modifiedUser.trim();
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getUseBatchId() {
        return useBatchId;
    }

    public void setUseBatchId(String useBatchId) {
        this.useBatchId = useBatchId == null ? null : useBatchId.trim();
    }

    public String getPassBatchId() {
        return passBatchId;
    }

    public void setPassBatchId(String passBatchId) {
        this.passBatchId = passBatchId == null ? null : passBatchId.trim();
    }

    public String getUseLogfield() {
        return useLogfield;
    }

    public void setUseLogfield(String useLogfield) {
        this.useLogfield = useLogfield == null ? null : useLogfield.trim();
    }

    public String getSharedFile() {
        return sharedFile;
    }

    public void setSharedFile(String sharedFile) {
        this.sharedFile = sharedFile == null ? null : sharedFile.trim();
    }
    
}