package com.msunsoft.main.etl.entity;


public class R_DIRECTORY {
    private int idDirectory;

    private int idDirectoryParent;

    private String directoryName;

    public int getIdDirectory() {
        return idDirectory;
    }

    public void setIdDirectory(int idDirectory) {
        this.idDirectory = idDirectory;
    }

    public int getIdDirectoryParent() {
        return idDirectoryParent;
    }

    public void setIdDirectoryParent(int idDirectoryParent) {
        this.idDirectoryParent = idDirectoryParent;
    }

    public String getDirectoryName() {
        return directoryName;
    }

    public void setDirectoryName(String directoryName) {
        this.directoryName = directoryName == null ? null : directoryName.trim();
    }
}