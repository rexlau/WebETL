package com.msunsoft.main.etl.entity;

import java.math.BigDecimal;
import java.util.Date;

public class ETL_LOG_JOB_METRIC {
    private String channelId;

    private String logDate;

    private String jobjobentry;

    private String result;

    private String parentChannelId;

    private String comments;

    private String reason;

    private String filename;

    private BigDecimal nr;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId == null ? null : channelId.trim();
    }

    public String getLogDate() {
        return logDate;
    }

    public void setLogDate(String logDate) {
        this.logDate = logDate;
    }

    public String getJobjobentry() {
        return jobjobentry;
    }

    public void setJobjobentry(String jobjobentry) {
        this.jobjobentry = jobjobentry == null ? null : jobjobentry.trim();
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result == null ? null : result.trim();
    }

    public String getParentChannelId() {
        return parentChannelId;
    }

    public void setParentChannelId(String parentChannelId) {
        this.parentChannelId = parentChannelId == null ? null : parentChannelId.trim();
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments == null ? null : comments.trim();
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason == null ? null : reason.trim();
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename == null ? null : filename.trim();
    }

    public BigDecimal getNr() {
        return nr;
    }

    public void setNr(BigDecimal nr) {
        this.nr = nr;
    }
}