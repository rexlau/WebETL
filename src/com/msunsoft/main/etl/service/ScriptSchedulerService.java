package com.msunsoft.main.etl.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.msunsoft.main.etl.entity.ETL_WORKJOB;
import com.msunsoft.main.etl.mapper.ETL_WORKJOBMapper;

@Service
@Transactional
public class ScriptSchedulerService {

	@Resource
	private ETL_WORKJOBMapper workjobMapper;
	
	/**
	 * 保存新添加的作业任务
	 */

	public void saveWorkJob(Map job){
		workjobMapper.insertSelective(job);
	}
	
	/**
	 * 查询作业任务
	 * @return 
	 */

	public List<ETL_WORKJOB> selectAllWorkJob(){
		return workjobMapper.selectETLAllWorkingJobs();
	}
}
