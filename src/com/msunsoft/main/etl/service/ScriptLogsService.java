package com.msunsoft.main.etl.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.msunsoft.main.etl.entity.ETL_LOG_JOBS;
import com.msunsoft.main.etl.entity.ETL_LOG_JOB_METRIC;
import com.msunsoft.main.etl.mapper.ETL_LOG_JOBSMapper;
import com.msunsoft.main.etl.mapper.ETL_LOG_JOB_METRICMapper;

@Service
@Transactional
public class ScriptLogsService {

	@Resource
	private ETL_LOG_JOBSMapper etlLogJob;
	
	@Resource
	private ETL_LOG_JOB_METRICMapper etlLogJobMetricMapper;
	/**
	 * 查询作业任务日志基本信息
	 */

	public List<ETL_LOG_JOBS> selectBaseETLJobLogs(Map etlLogJobs){
		return etlLogJob.selectBaseETLJobLogs(etlLogJobs);
	}
	
	/**
	 * 查询作业任务日志详细信息
	 */

	public ETL_LOG_JOBS selectETLJobLogsBlob(Map etlLogJobs){
		return etlLogJob.selectETLJobLogsBlob(etlLogJobs);
	}
	
	/**
	 * 查询作业度量信息
	 * @return 
	 */

	public List<ETL_LOG_JOB_METRIC> selectETLJobMetrics(Map etlLogJobs){
		return etlLogJobMetricMapper.selectETLJobMetrics(etlLogJobs);
	}
}
