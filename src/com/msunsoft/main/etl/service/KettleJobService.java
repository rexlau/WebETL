package com.msunsoft.main.etl.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.msunsoft.main.etl.entity.R_JOB;
import com.msunsoft.main.etl.mapper.R_JOBMapper;

@Service
@Transactional
public class KettleJobService {

	@Resource
	private R_JOBMapper jobMapper;
	
	@Resource 
	private KettleDirectoryService kettleDirectoryService;

	/**
	 * 获取资源库中所有作业JOB信息
	 */

	public List<R_JOB> getJobALL() {
		List<R_JOB> jobList = jobMapper.selectKettleJob(null);
		for(R_JOB job : jobList){
			String path = kettleDirectoryService.getAbsolutePath(job.getIdDirectory());
			job.setAbsolutePath(path);
		}
		return jobList;
	}
	
}
