package com.msunsoft.main.etl.engine;

import javax.annotation.Resource;

import org.pentaho.di.core.KettleEnvironment;
import org.pentaho.di.core.database.DatabaseMeta;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.job.Job;
import org.pentaho.di.job.JobMeta;
import org.pentaho.di.repository.LongObjectId;
import org.pentaho.di.repository.ObjectId;
import org.pentaho.di.repository.kdr.KettleDatabaseRepository;
import org.pentaho.di.repository.kdr.KettleDatabaseRepositoryMeta;
import org.springframework.stereotype.Service;

import com.msunsoft.main.etl.entity.ETL_WORKJOB;

/**
 * kettle 运行环境引擎，运行job
 * @author Administrator
 *
 */
@Service
public interface KettleEnvironmentRunEngineService {
	
	/**
	 * 运行Kettle job 作业脚本
	 * @param WORKID
	 * @return
	 */
	public boolean runJobformation(Long id_job);
	
	/**
	 * 停止kettle Job作业脚本
	 * @param WORKID
	 * @return
	 */
	public boolean stopJobformation(Long id_job);
	
	/**
	 * 暂停kettle job 作业脚本
	 * @param WORKID
	 * @return
	 */
	public boolean pauseTransformation(Long id_job);

	/**
	 * 获取指定job作业状态
	 * @param long1
	 * @return 
	 */
	public String getJobStatusById(Long id_job);
}
