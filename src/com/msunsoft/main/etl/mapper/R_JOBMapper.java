package com.msunsoft.main.etl.mapper;

import com.msunsoft.main.etl.entity.R_JOB;
import com.msunsoft.main.etl.entity.R_JOBWithBLOBs;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

public interface R_JOBMapper {
    int deleteByPrimaryKey(BigDecimal idJob);

    int insert(R_JOBWithBLOBs record);

    int insertSelective(R_JOBWithBLOBs record);


    int updateByPrimaryKeySelective(R_JOBWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(R_JOBWithBLOBs record);

    int updateByPrimaryKey(R_JOB record);
    /**
     * 查询作业信息
     * @param Job
     * @return
     */
    List<R_JOB> selectKettleJob(R_JOB job);
}