package com.msunsoft.main.etl.mapper;

import java.util.List;
import java.util.Map;

import com.msunsoft.main.etl.entity.ETL_LOG_JOBS;

public interface ETL_LOG_JOBSMapper {
	/**
	 * 查询日志信息（不含blob字段）
	 * @param etlLogJobs
	 * @return
	 */
	List<ETL_LOG_JOBS> selectBaseETLJobLogs(Map etlLogJobs);
	
	/**
	 * 查询详细日志信息（只含blob字段）
	 * @param etlLogJobs
	 * @return
	 */
	ETL_LOG_JOBS selectETLJobLogsBlob(Map etlLogJobs);
	
}