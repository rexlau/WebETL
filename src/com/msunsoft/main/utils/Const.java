package com.msunsoft.main.utils;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import org.pentaho.di.core.KettleEnvironment;
import org.pentaho.di.core.database.DatabaseMeta;
import org.pentaho.di.core.exception.KettleException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Const {
	public static final String LOG_JOBS = "ETL_LOG_JOBS";//作业日志
	public static final String LOG_JOBS_STEP = "ETL_LOG_JOBS_STEP";//步骤日志
	public static final String LOG_JOBS_CHANNEL = "EtL_LOG_JOB_CHANNEL";
	public static final String LOG_TRANS = "ETL_LOG_TRANS";//转换日志
	public static final String LOG_TRANS_STEP = "ETL_LOG_TRANS_STEP";//转换步骤日志
	public static DatabaseMeta logTableDatabaseMeta;
	
	public static final String JOB_STATUS_START = "开始运行";
	public static final String JOB_STATUS_END = "运行结束";
	public static final String JOB_STATUS_STOP = "停止运行";
	public static final String JOB_STATUS_ERROR = "运行错误";
	public static final String JOB_STATUS_RUNNING = "正在运行";
	public static final String JOB_STATUS_PAUSED = "暂停运行";
	/*常见返回值*/
	public static enum HTTPRESPONSE{
		
		NO_DATA("NO_DATA"),
		;
		
		public String id;
		private HTTPRESPONSE(String id) {
			this.id = id;
		}

		public String toString() {
			return id;
		}
	}

	public static ApplicationContext WEB_APP_CONTEXT = null; //该值会在web容器启动时由WebAppContextListener初始化
	
	public static DatabaseMeta getLogTableDatabaseMeta() {
		try {
			KettleEnvironment.init();
		} catch (KettleException e) {
			e.printStackTrace();
		}
		if(logTableDatabaseMeta != null)
			return logTableDatabaseMeta;
		else{
			
			logTableDatabaseMeta = (DatabaseMeta) WEB_APP_CONTEXT.getBean("etl.databaseMeta");
			return logTableDatabaseMeta;
		}
	}

	
	/**
     * Check if the string supplied is empty.  A String is empty when it is null or when the length is 0
     * @param string The string to check
     * @return true if the string supplied is empty
     */
    public static final boolean isEmpty(String string)
    {
    	return string==null || string.length()==0;
    }
    
    /**
     * Check if the stringBuffer supplied is empty.  A StringBuffer is empty when it is null or when the length is 0
     * @param string The stringBuffer to check
     * @return true if the stringBuffer supplied is empty
     */
    public static final boolean isEmpty(StringBuffer string)
    {
    	return string==null || string.length()==0;
    }
    
    /**
     * Check if the string array supplied is empty.  A String array is empty when it is null or when the number of elements is 0
     * @param string The string array to check
     * @return true if the string array supplied is empty
     */
    public static final boolean isEmpty(String[] strings)
    {
        return strings==null || strings.length==0;
    }

    /**
     * Check if the array supplied is empty.  An array is empty when it is null or when the length is 0
     * @param array The array to check
     * @return true if the array supplied is empty
     */
    public static final boolean isEmpty(Object[] array)
    {
     return array==null || array.length==0;
    }
    
    /**
     * Check if the list supplied is empty.  An array is empty when it is null or when the length is 0
     * @param list the list to check
     * @return true if the supplied list is empty
     */
    public static final boolean isEmpty(List<?> list)
    {
     return list==null || list.size()==0;
    }
	
    /**
     * Returns a string of the stack trace of the specified exception
     */
    public static final String getStackTracker(Throwable e)
    {
    	return getClassicStackTrace(e);
    }
    
    public static final String getClassicStackTrace(Throwable e)
    {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        e.printStackTrace(printWriter);
        String string = stringWriter.toString();
        try { stringWriter.close(); } catch(IOException ioe) {} // is this really required?
        return string;
    }
}
